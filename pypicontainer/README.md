# PyPiServer

How to setup PyPI server using docker

## Prerequisites:

Our sandbox will be virtual enviroment (virtual machine) and it can be Docker
for Windows or Oracle VirtualBox. 

Be aware that Docker for Windows *need Hyper-V* to work, but Oracle VB *does
not* work with Hyper-V enabled !!

If you want to use both platform you must enable or disable (Turn Windows
features off or on --> Hyper-V and Windows Hypervisor Platform) according to needs. 

### VirtalBox network

We want to have access to the guest machine, and guest machine must be
connected to the internet.

An easy solution is to use a *Brigded Adapter*, but then you must use the actual address from your LAN.
In this case, contact your network administrator for the IP address. 

The other solution is using *NAT Network* and setup *Port Forwarding*
between host and guest ports. 

### Set git accounts identity 
```
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
```
If you don't want to enter credentials every time use:
```
git config --global credential.helper store
```
Credentials are stored in *~/.git-credential* as plain-text. 
If you want to change it later, just remove *git-credential*.

1. Create repository in GitLab
2. Clone on the local host:
``` 
https://gitlab.com/nbabajic/pypiserver.git
```

## Install PyPi server 
### Standard method 

```
$ sudo pip3 install pypiserver
$ mkdir ~/packages
``` 
Copy some packgage (for test) into *~/package* folder and 
start pypi server in the backgrond nad listennig on port 8080
 ``` 
$ pypi-server -p 8080 ~/packages &
``` 
Check if server is running and 
listening on port 8080: 
```
$ netstat -tunelp 
...
tcp   0 0 0.0.0.0:8080    0.0.0.0:*   LISTEN    10077/python3
...
```
Open web page *http://localhost:8308* (depend of your VirtualBox Port Forwarding in NAT Network) 

We can see what pypi server looks like and what to expect.
If everything is fine, you can go to install pypi server using docker.

#### Install pypi server using Docker

Create directory pypidocker
```
$ mkdir pypidocker
$ cd pypidocker
```

PyPi server requires Python 2.7 or Python 3.4+, so we gonna use base image
with Python.

##### Use Python:alpine base image
Create Docker file Dockerfile.alpine with the following contest:
```
# Ligthweight  image
FROM python:3.7-alpine

#Install pypi server
RUN pip3 install pypiserver

# Create folder for packages
RUN mkdir -p /data/packages

# Add empty, dummy package 
RUN touch /data/packages/dummy_pkg.tar.gz 

# This will not publish port, only as reference 
EXPOSE 8081

# Start pypi server in container

ENTRYPOINT ["pypi-server", "-p", "8080", "/data/packages"]
```

Build image:

```
$ time sudo docker build . -f Dockerfile.alpine -t alpineimg

real    0m19.898s
user    0m0.036s
sys     0m0.045s

```

Check images:
```
$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
alpineimg           latest              532b7c41f4aa        4 minutes ago       47.3MB
python              3.7-alpine          295b051ee125        2 weeks ago         41.7MB
```

Run container and setting local port 8081:
```
$ docker run -d -p 8081:8080 --name alpinecont alpineimg

$ docker container ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                              NAMES
83c2adffb694        alpineimg           "pypi-server -p 8080…"   3 minutes ago       Up 3 minutes        8081/tcp, 0.0.0.0:8081->8080/tcp   alpinecont

```
Open http://localhost:8081 (again, check NAT in VirtualBox !!!) and dummy_pkg.tar.gz should be there.

It is nice, image is only 47MB, build takes less then 20 sec. But, there is recommendation not to use Alpine for Python !?
Why? The explanation is as follows: many Linux distribution  use the GNU version (*glibc*) of the standard C library, but Alpine Linux uses *musl* and it 
can lead to strange  run-time bugs for some programs, bigger image if compile new programs, etc. 

So we will try ubuntu as base image. 

##### Use Ubuntu 18.04 as base image
Create Docker file Dockerfile.ubuntu with the following contest:
```
FROM ubuntu:18.04

# Standard Ubuntu image update
RUN apt-get update && apt-get install -y \
python3.7 \
python3-pip

# Install pypi server
RUN pip3 install pypiserver

# Create package folder
RUN mkdir -p /data/packages && \
    touch /data/packages/dummy_ubuntupkg.tar.gz

# Not publish port, only as reference
EXPOSE 8082

# Start pypi server
ENTRYPOINT ["pypi-server", "-p", "8080", "/data/packages"]

```

Build image:

```
$ time sudo docker build . -f Dockerfile.ubuntu -t ubuntuimg

real    4m2.826s
user    0m0.112s
sys     0m0.119s

```

Check images:
```
$ docker images

REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
ubuntuimg           latest              df79c01b0c7a        49 seconds ago      499MB
alpineimg           latest              532b7c41f4aa        33 minutes ago      47.3MB
ubuntu              18.04               c14bccfdea1c        6 days ago          63.2MB
python              3.7-alpine          295b051ee125        2 weeks ago         41.7MB

```

Run container and setting local port 8082:
```
$ docker run -d -p 8082:8080 --name ubuntucont ubuntuimg
```
Open http://localhost:8082 (again, check NAT in VirtualBox !!!) and
dummyubuntu_pkg.tar.gz should be there.


### Summarry
 Alpine image is smaller, build process is faster, but there are opinions against
its use.

Standard Ubuntu image are bigger about 10X (47 Mb vs 499 Mb), initial build, without
 cache, is also slower about 10X (19Sec vs 4 min).

After all, the decision is up to you.

### Add volume

Our new pypi server works fine, but if container crashes or being removed all packages stored in container will be lost !
Package directory */data/packages/* exist only if container is alive. If we delete container, this folder is also deleted.

We can add some kind of backup procedure inside the container, but it requires additional efforts and image can be bigger and more complex. 
Beter solution is to make data "visible" to the host machine.

There are two options to persist files - *volumes* and *mount*. We will use the first option.

To add volume to the container use *-v* flag:

```
sudo docker run --rm  -d --name pypicont  -p 8081:8080 -v /home/user/pypiserver/pypibackup/:/data/packages  ubuntuimg
```
*-v* flag means "link host folder */home/user/pypiserver/pypibackup/* with */data/package* folder in the container"

We can put any file in */home/user/pypiserver/pypibackup/* and check what is in the container.
```
$ docker exec -it  pypicont ls -la /data/packages

```

This method works, but there are some drawback.
RUN mkdir command adds new layers on the top of the image, increasing its size. Folder created on that way can not be shared between different container.
Volumes are the best way to persist data in Docker.

One way is to use VOLUME command in dockerfile. 

Replace 
```
RUN mkdir -p /data/packages 
 touch /data/packages/dummy_ubuntupkg.tar.gz
```
with 
```
VOLUME /data/packages
``` 

and then run *docker build* command with flag *-v*, as in last example.

It is also posible to create volume using Docker CLI:
```
$ docker create volume pypkg
```
**pypkg** volume is created in **/var/lib/docker/volumes/pypkg**

Run container with *--mount* flag: 
```
sudo docker run  --rm -d --name pypicontvol  -p 8081:8080  --mount source=pypkg,destination=/data/packages ubuntuimg
```

If file or directory to be used does not exist *-v* flag always **creates
as directory**.
**--mount** flag will **not** create it, but generate an error. 

We can run other container using the same volume and data will be available in both containers, as well as on the host machine.


