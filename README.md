# How to start multiple services using Docker Compose

## Workshop file structure

```

├── docker-compose.yml
├── jenkinscontainer
│   ├── Dockerfile
│   ├── Dockerfile.chown
│   ├── Dockerfile.entry
│   ├── Dockerfile.root
│   ├── entrypoint_ch.sh
│   ├── entrypoint_gosu.sh
│   ├── entrypoint.sh
│   ├── keepalive.sh
│   └── README.md
├── pypicontainer
│   ├── Dockerfile
│   └── README.md
└── README.md

3 directories, 15 files

```

We have created images for *pypi server* and *Jenkins*. Now we want to
 manage both of them at once.

*Docker Compose* is a tool that lets us to manage multiple Docker container.

## Prerequisites:

> NOTE:  See [*pypi* server](../pypicontainer/README.md) for virtual machine
> and network setup !!!!

Docker Compose does not build Docker images explicitly, so we must follow instruction 
for [*pypi* server](../pypicontainer/README.md) and [Jenkins](../jenkinscontainer/README.md)

Docker Cmposer is official Docker tool, but it is not part of Docker
installation and it must be installed as a separated packet.
To install Docker Compose on the Ubuntu use following command:
```
$ sudo apt-get install docker-compose
```

if you can't start *docker-compose* after successful instalation check
permision for *docker-compose* and change it:
```
$ sudo chmod u+x /usr/local/bin/docker-compose
```

What is a Dockerfile for Docker, it is **Docker Compose file** for Docker Compose. Docker Compose file 
uses **YAML** syntax and have to be saved as **docker-compose.yml**. As YAML file it constist of many 
key-value pairs and directives. Most used top-level objects are:

- Version
- Services
- Network
- Volume

## Create simple docker-compose.yml
There are Dockerfiles for [*pypi*](pypiconatiner/Dockerfile]
 and [*Jenkins*](jenkinscontainer/Dockerfile) and we only  need to map
*docker run* parameters in to the *docker-compose.yml*

For example, 
```
$ docker run --rm -d --name pypi_container  -p 8081:8080  --mount source=pypkg,destination=/data/packages ubuntuimg 
```

should be mapped to:
```
services:
  pypiserver:
    container_name: pypi_container
    build:
      context: .
      dockerfile: ./pypidocker/Dockerfile
    volumes:
      - /data/packages:/data/packages
    ports:
      - '8081:8080'  
```
and *docker run* for Jenkins:
```
$ docker run --rm -d -p 8080:8080 --name jenkins_container -v jenkins_vol:/var/jenkins_home jenkinsimage
```

should be mapped to:

```
services:
...
  jenkins:
    container_name: jenkins_container
    build:
      context: .
      dockerfile: ./jenkinsdocker/Dockerfile
    volumes:
      - jenkins_vol:/var/jenkins_home   
    ports:
      - '5000:5000'
      - '8080:8080'

volumes:
  jenkins_vol:
    driver: local 
```
#### docker-compose naming convention

Docker Compose use folder name as prefix for naming convention, in our case *workshop1*.
After *docker-compose up* will be created following containers:
- workshop1_pypiserver_1 
- workshop1_jenkins_1.

If we want to change name must use apropriate key *container_name*:
```
...  
  pypiserver:
  container_name: pypi_container
  ...
  jenkins:
    container_name: jenkins_container

```      

The same case is with volume name. If you want to change volume name, first
must create volume using:
```
$ docker volume create jenkins_vol 
```
then add fo to the *docker-compose.yml*:
```
volumes:
  jenkins_vol:
    external:true          
```

> *name* key is added version 3.4 file format 
>```
>...
>    volumes:
>      - jenkins_vol:/var/jenkins_home   
> ...
> volumes:
>   jenkins_vol:
>     name: jenkins_vol
>```

Image name can be specified using *image* key:
```
services:
  pypiserver:
    container_name: pypi_container
    build: ./pypicontainer
    image: pypiimage:1.0    
  
...
```
Finally, to build *[docker-compose file](./docker-compose.yml)* use following command:
```
$ docker-compose build
```
 then run in the background:
```
$ docker-compose up -d
Creating pypi_container ...
Creating jenkins_container ...
Creating pypi_container
Creating jenkins_container ... done
```
check if it is running:
```
$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                                            NAMES
2ecd8d05f652        jenkinsimage:1.0    "/entrypoint.sh jenk…"   2 minutes ago       Up 2 minutes        0.0.0.0:5000->5000/tcp, 0.0.0.0:8080->8080/tcp   jenkins_container
c252093485e1        pypiimage:1.0       "pypi-server -p 8080…"   2 minutes ago       Up 2 minutes        0.0.0.0:8081->8080/tcp                           pypi_container
```
check host ports:
```
...
tcp6       0      0 :::8080                 :::*                    LISTEN      -
tcp6       0      0 :::8081                 :::*                    LISTEN      -
...
tcp6       0      0 :::5000                 :::*                    LISTEN      -
...
```

Now can use *PyPi server* at **http://localhost:8081** and *Jenkins* at
**http://localhost:8080***  
