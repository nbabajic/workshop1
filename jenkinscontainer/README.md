# How to setup Jenkins using Dockerfile

## Prerequisites:

See [general notice](../README.md).

Base image is Ubuntu 18.04.

Jenkins needs **Java 8 runtime environment** (it supported Java 10 and 11
also).
Ubuntu image is about 60MB, but if install Java JDK 11 it will grow to 700MB.
Better solution is to install Java JRE, so final size is about 500MB.

Standard procedure for Jenkins installation is easy:
 
```
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > \
    /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
sudo apt-get install jenkins
```
After the installation is completed, Jenkins is **automatically started** using **systemctl**.

We only need to "translate" these commands to the Dockerfile.

### Create docker file

Create directory jenkinsdocker
```
$ mkdir jenkinsdocker
$ cd jenkinsdocker
```

##### Create Dockerfile

Use ubuntu:18.04 as base image and create Dockerfile:
```
# Simple dockerfile
FROM ubuntu:18.04
# gnupg2 is required by apt-key
RUN apt-get update && apt-get install -y \
gnupg2 \
openjdk-8-jre \
wget

# Add the Jenkins Debian repo
RUN  wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | apt-key add -

# Append Debian package repo to the servers's source list
RUN sh -c 'echo deb http://pkg.jenkins-ci.org/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'

RUN apt-get update
EXPOSE 8080

# Install and start Jenkins and its dependencies
RUN apt-get install jenkins
```

Build and run image:

```
$ docker build . -t test

$ docker run  -d -p 8081:8080 --name jenkinsc test
438aba7aa3e122bb3fb8c935c5b506f4b20b7e68bd27b3ffe0504aefff3d7d55

$ docker container ps -la
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                                            PORTS               NAMES
438aba7aa3e1        test           "/bin/bash"         3 seconds ago       Exited (0) 2 seconds                        ago                       jenkinsc
```

There are no errors, but there is no running container either?

Reason is simple: Docker image **does not support** systemctl. There are several solutiones like docker-system-replacement, 
supervisord, images with enabled init system. But, it is recommended to use one service per container, 
so we must find a way to start Jenkins without systemctl.

Luckily for us, Jenkins can be started using Java

``` 
java -jar jenkins.war
```

Tricky part can be where is jenkins.war?

Simply, run the container in interactive mode and search:
```
$ docker run  --rm -it test find . -name jenkins.war
./usr/share/jenkins/jenkins.war
```
Now we can add 
```
CMD ["java", "-jar", "/usr/share/jenkins/jenkins.war"]

```

*build image* and *run container*. Container now works fine and it looks correct.

But, there are drawbacks - service jenkins, inside container,  is started as root and how backup data?
For backup we will use *VOLUME* instruction and for user *USER* instruction.

Hence, solution would be to add VOLUME and USER to the Dockerfile:

```
FROM ubuntu:18.04
RUN apt-get update && apt-get install -y \
gnupg2 \
openjdk-8-jre \
wget

# Add the Jenkins Debian repo
RUN  wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | apt-key add -
# Append Debian package repo to the servers's source list
RUN sh -c 'echo deb http://pkg.jenkins-ci.org/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
RUN apt-get  update

# Set user and env variables for Jenkins - used at run timr, 
# not at Jenkins installation
ARG JENKINS_HOME=/var/jenkins_home
ENV JENKINS_HOME=$JENKINS_HOME
VOLUME $JENKINS_HOME

# Install Jenkins
RUN apt-get  install jenkins -y

# Start jenkins as jenkins user 
USER jenkins
CMD ["java", "-jar", "/usr/share/jenkins/jenkins.war"]
```

add volume

```
$ docker volume create jenkinsvol
```

and finally *build* image and *run* container using **-v** flag.

Container now doesn't works again  and we get error:
**java.io.IOException: Permission denied**

The solution can be to *chown* for /var/jenkins_home, but it has not effect in Dockerfile.
**VOLUME** and **RUN mkdir** are not the same.
**RUN mkdir** works as expected and we can *chown* so created dirs.

After issuing **VOLUME** instruction in Dockerfile it is not possible to change that volume !!!

The following commands will not work:  
```
VOLUME $JENKINS_HOME 
RUN chown -R jenkins $JENKINS_HOME
```
We expect to *chown* image's file system, but it is actually running in the volume of
the temporary container.

There are two common used solutions:
```
... 
RUN mkdir $JENKINS_HOME && chown -R jenkins:jenkins $JENKINS_HOME
VOLUME $JENKINS_HOME
... 
```

Complete Dockerfile can be found [here](./Dockerfile.chown) and entrypoint
script [here](./entrypoint_ch.sh). 

Second solution is to use **gosu** utility. It is simple utility,similar to
*sudo*, but without strange behavior. It is often used in ENTRYPOINT script to manage root privilegies.

[Docker file](./Dockerfile.gosu) now looks like:

```
...
# Set Jenkins env variables 
ARG JENKINS_HOME=/var/jenkins_home
ENV JENKINS_HOME=$JENKINS_HOME

# Setup dir and ownership
VOLUME $JENKINS_HOME

# Install Jenkins 
RUN apt-get install jenkins -y
 
EXPOSE 8080

# Start jenkins 

COPY entrypoint_gosu.sh /entrypoint.sh
RUN chmod u+x /entrypoint.sh
RUN chown jenkins:jenkins /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
CMD ["jenkins","java", "-jar", "/usr/share/jenkins/jenkins.war"]
```
and [entrypoint_gosu.sh](./entrypoint_gosu.sh) looks like:
```
#!/bin/bash
set -e
if [ "$1" = 'jenkins' ]; then
    chown -R jenkins:jenkins "$JENKINS_HOME"
    exec gosu "$@"
fi
exec "$@"
```
*run* container:
```
$ sudo docker run --rm -d -p 8080:8080 --name jenkins -v jenkinsvol:/var/jenkins_home test.gosu
64c83247b4a992482c8e37b525f74b269529512a6e203e8ecb50fb8a0ec5677d
```
Finnaly, check who started Jenkins:

```
$ docker exec -it jenkins ps -aux
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
jenkins      1 53.0  9.2 2335260 93648 ?       Ssl  23:00   0:02 java -jar /usr/share/jenkins/jenkins.war
root        29  0.0  0.2  34404  2844 pts/0    Rs+  23:00   0:00 ps -aux
```

Everything is fine, we can backup jenkins data (/var/lib/docker/volumes/jenkinsvol ) and jenkins user starts
Jenkins 

